<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ProjetRepository;
use App\Repository\UserRepository;
use App\Entity\Projet;
use App\Form\ProjetType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

use Doctrine\ORM\EntityManagerInterface;


class ProjetController extends AbstractController
{
    /**
     * @Route("/", name="app_home")
     */
    public function index(ProjetRepository $repo): Response
    {
      $projects_list = $repo->findBy([], ['createdAt' => 'DESC' ]);

      return $this->render('projet/index.html.twig', compact('projects_list') );
    }


    /**
    * @Route("/project", name="app_project_create")
    */
    public function create(Request $request, EntityManagerInterface $em, UserRepository $userrepo): Response
    {
      $projet = new Projet;

      $form = $this->createForm(ProjetType::class, $projet);

      $form->handleRequest($request);

      if($form->isSubmitted() && $form->isValid())
      {
        // User owner seting
        $projet->setUser($this->getUser());

        $projet->setCreatedAt(new \DateTimeImmutable);
        $em->persist($projet);
        $em->flush();

        $this->addFlash("success", "Project Successfully Created");

        return $this->redirectToRoute('app_home');
      }

      return $this->render('projet/create.html.twig', [
        'form' => $form->createView()
      ]);
    }

    /**
    * @Route("/project/{id<[0-9+]>}/show", name="app_project_show")
    */
    public function show(Projet $projet): Response
    {
      return $this->render('projet/show.html.twig', compact('projet'));
    }


    /**
    * @Route("/project/{id<[0-9+]>}/edit", name="app_project_edit")
    */
    public function edit(Projet $projet, Request $request, EntityManagerInterface $em): Response
    {
      $form = $this->CreateForm(ProjetType::class, $projet);

      $form->handleRequest($request);

      if($form->isSubmitted() && $form->isValid())
      {
        $em->flush();

        $this->addFlash("success", "Project Successfully edited ");

        return $this->redirect($this->generateUrl('app_project_show', ['id' => $projet->getId() ]));
      }

      return $this->render('projet/edit.html.twig', ['form' => $form->createView() ]);
    }


    /**
    * @Route("/project/{id<[0-9+]>}/delete", name="app_project_delete")
    */
    public function delete(Projet $projet, Request $request, EntityManagerInterface $em): Response
    {
      $form = $this->createFormBuilder()
                   ->add('name')
                   ->add('password', PasswordType::class)
                   ->getForm();
      $form->handleRequest($request);

      if($form['name']->getData() === 'sudo' && $form['password']->getData() === 'sudo')
      {
        $em->remove($projet);
        $em->flush();

        $this->addFlash("info", "Project successfully deleted");

        return $this->redirectToRoute('app_home');
      }


      return $this->render('projet/delete.html.twig', ['form' => $form->createView() ]);

    }
}
