<?php

namespace App\Form;

use App\Entity\Projet;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;


class ProjetType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('imageFile', VichImageType::class, [
            'required' => false,
            'allow_delete' => true,
            'delete_label' => 'Delete picture',
            // 'download_label' => '...',
            'download_uri' => false,
            'image_uri' => true,
            'imagine_pattern' => 'square_thumbnail_edit',
            'asset_helper' => true,
            ])
            //->add('imageName')
            ->add('title', TextType::class,[
              'label' => 'Title *',
              'help' => 'Title may contain more than 3 characters and must not be empty',
            ])
            ->add('description', TextareaType::class,[
              'label' => 'Description *',
              'help' => 'Description may contain more than 10 characters and must not be empty',
            ])
            ->add('endAt')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Projet::class,
        ]);
    }
}
